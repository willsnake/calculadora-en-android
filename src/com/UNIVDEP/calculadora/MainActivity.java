package com.UNIVDEP.calculadora;

import android.os.Bundle;
import android.app.Activity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.view.View.*;
import android.widget.*;

public class MainActivity extends Activity implements OnClickListener {

	// Se toman los campos de texto
	EditText etNum1;

	// Se toma el boton creado
	Button btnSum;
	Button btnRes;
	Button btnMul;
	Button btnDiv;
	Button btnPor;
	Button btnSqr;
	Button btnNep;
	Button btnLn;
	Button btnLog;
	Button btnLogx;

	// Se toma el texto de resultado
	TextView tvResult;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Se buscan los elementos que corresponden a sus variables
		etNum1 = (EditText) findViewById(R.id.etNum1);

		btnSum = (Button) findViewById(R.id.btnSum);
		btnRes = (Button) findViewById(R.id.btnRes);
		btnMul = (Button) findViewById(R.id.btnMul);
		btnDiv = (Button) findViewById(R.id.btnDiv);
		btnPor = (Button) findViewById(R.id.btnPor);
		btnSqr = (Button) findViewById(R.id.btnSqr);
		btnNep = (Button) findViewById(R.id.btnNep);
		btnLn = (Button) findViewById(R.id.btnLn);
		btnLog = (Button) findViewById(R.id.btnLog);
		btnLogx = (Button) findViewById(R.id.btnLogx);

		tvResult = (TextView) findViewById(R.id.tvResult);

		// Se le crea un listener al botón de la raíz
		btnSum.setOnClickListener((OnClickListener) this);
		btnRes.setOnClickListener((OnClickListener) this);
		btnMul.setOnClickListener((OnClickListener) this);
		btnDiv.setOnClickListener((OnClickListener) this);
		btnPor.setOnClickListener((OnClickListener) this);
		btnSqr.setOnClickListener((OnClickListener) this);
		btnNep.setOnClickListener((OnClickListener) this);
		btnLn.setOnClickListener((OnClickListener) this);
		btnLog.setOnClickListener((OnClickListener) this);
		btnLogx.setOnClickListener((OnClickListener) this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	public void onClick(View v) {

		// Se crean las variables que van a contener los valores
		float num1 = 0;
		float num2 = 0;
		float nep = (float) Math.E;
		float result = 0;

		// Se revisa si los campos están vacíos
		if (TextUtils.isEmpty(etNum1.getText().toString())) {
			return;
		}

		// Se toman los valores de los campos de texto, y se convierten en
		// numeros
		num1 = Float.parseFloat(etNum1.getText().toString());
		num2 = Float.parseFloat(tvResult.getText().toString());

		// Se detecta si el usuario ha tocado el botón, si lo hace, se manda a
		// hacer la operación.
		// Aqui es donde se van a agregar las demás operaciones
		switch (v.getId()) {
		case R.id.btnSum:
			result = num1 + num2;
			etNum1.setText("");
			break;
		case R.id.btnRes:
			result = num2 - num1;
			etNum1.setText("");
			break;
		case R.id.btnMul:
			result = num1 * num2;
			etNum1.setText("");
			break;
		case R.id.btnDiv:
			result = num2 / num1;
			etNum1.setText("");
			break;
		case R.id.btnPor:
			
			result = (float) ((float) num2 * (num1 * .01)); 
			break;
		case R.id.btnSqr:
			result = (float) Math.sqrt(num1);
			etNum1.setText("");
			break;
		case R.id.btnNep:
			etNum1.setText(Float.toString(nep));
			break;
		case R.id.btnLn:
			result = (float) Math.log(num1);
			break;
		case R.id.btnLog:
			result = (float) Math.log10(num1);
			break;
		case R.id.btnLogx:
			num1 = (float) Math.log10(num1);
			num2 = (float) Math.log10(num2);
			result = num2 / num1;
			break;
		default:
			break;
		}

		// Se imprime el resultado en la caja de texto
		tvResult.setText(Float.toString(result));
	}

}
